import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: _RowsOrColumns(),
    );
  }
}

class _RowsOrColumns extends StatefulWidget {
  @override
  _RowsOrColumnsState createState() => _RowsOrColumnsState();
}

class _RowsOrColumnsState extends State<_RowsOrColumns> {
  MainAxisAlignment _mainAxisAlignment = MainAxisAlignment.start;
  CrossAxisAlignment _crossAxisAlignment = CrossAxisAlignment.center;

  bool _useRow = true;

  void _setMainAxis(MainAxisAlignment main) {
    setState(() {
      _mainAxisAlignment = main;
    });
  }

  void _setCrossAxis(CrossAxisAlignment cross) {
    setState(() {
      _crossAxisAlignment = cross;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          if (_useRow)
            Row(
              mainAxisAlignment: _mainAxisAlignment,
              crossAxisAlignment: _crossAxisAlignment,
              textBaseline: TextBaseline.alphabetic,
              children: [
                _Item(color: Colors.red),
                _Item(color: Colors.blue, isBig: true),
                _Item(color: Colors.green),
              ],
            )
          else
            Expanded(
              child: Column(
                mainAxisAlignment: _mainAxisAlignment,
                crossAxisAlignment: _crossAxisAlignment,
                textBaseline: TextBaseline.alphabetic,
                children: [
                  _Item(color: Colors.red),
                  _Item(color: Colors.blue, isBig: true),
                  _Item(color: Colors.green),
                ],
              ),
            ),
          // Text(
          //   'Main\naxis\nalignment',
          //   style: TextStyle(
          //     fontWeight: FontWeight.w900,
          //     fontSize: 15,
          //     color: Colors.red,
          //     decoration: TextDecoration.underline,
          //     decorationStyle: TextDecorationStyle.wavy,
          //   ),
          //   textAlign: TextAlign.center,
          // ),
          Text('Main axis alignment'),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                onPressed: () => _setMainAxis(MainAxisAlignment.start),
                child: Text('start'),
              ),
              ElevatedButton(
                onPressed: () => _setMainAxis(MainAxisAlignment.center),
                child: Text('center'),
              ),
              ElevatedButton(
                onPressed: () => _setMainAxis(MainAxisAlignment.end),
                child: Text('end'),
              ),
              ElevatedButton(
                onPressed: () => _setMainAxis(MainAxisAlignment.spaceBetween),
                child: Text('spaceBetween'),
              ),
              ElevatedButton(
                onPressed: () => _setMainAxis(MainAxisAlignment.spaceAround),
                child: Text('spaceAround'),
              ),
              ElevatedButton(
                onPressed: () => _setMainAxis(MainAxisAlignment.spaceEvenly),
                child: Text('spaceEvenly'),
              ),
            ],
          ),
          Text('Cross axis alignment'),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: [
              ElevatedButton(
                onPressed: () => _setCrossAxis(CrossAxisAlignment.start),
                child: Text('start'),
              ),
              ElevatedButton(
                onPressed: () => _setCrossAxis(CrossAxisAlignment.center),
                child: Text('center'),
              ),
              ElevatedButton(
                onPressed: () => _setCrossAxis(CrossAxisAlignment.end),
                child: Text('end'),
              ),
              ElevatedButton(
                onPressed: () => _setCrossAxis(CrossAxisAlignment.baseline),
                child: Text('baseline'),
              ),
              ElevatedButton(
                onPressed: () => _setCrossAxis(CrossAxisAlignment.stretch),
                child: Text('stretch'),
              ),
            ],
          ),
          Text('Row/Column'),
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            children: [
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    _useRow = true;
                  });
                },
                child: Text('Row'),
              ),
              ElevatedButton(
                onPressed: () {
                  setState(() {
                    _useRow = false;
                  });
                },
                child: Text('Column'),
              ),
            ],
          )
        ],
      ),
    );
  }
}

class _Item extends StatelessWidget {
  final bool isBig;
  final Color? color;

  const _Item({this.isBig = false, this.color, Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final size = isBig ? 120.0 : 60.0;
    return Container(
      width: size,
      height: size,
      color: color,
    );
  }
}
